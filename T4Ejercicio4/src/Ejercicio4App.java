
public class Ejercicio4App {

	public static void main(String[] args) {

		int n = 10;
		System.out.println("Valor inicial N = " + n);
		//Incrementar 77
		n+= 77;
		System.out.println("N + 77 = " + n);
		//Decrementar 3
		n-= 3;
		System.out.println("N - 3 = " + n);
		//Duplicar
		n*= 2;
		System.out.println("N*2 = " + n);

	}

}
